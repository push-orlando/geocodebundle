<?php

namespace Push\GeocodeBundle;

use Doctrine\ORM\EntityManager;
use Push\GeocodeBundle\Entity\Coordinates;
use Push\GeocodeBundle\Provider\LocationInterface;
use Push\GeocodeBundle\Provider\GoogleProvider;
use Push\GeocodeBundle\Provider\GeocodeIOProvider;
use Push\GeocodeBundle\Provider\NoResultsException;

class Geocoder
{
    protected $em;
    protected $locationsRepo;
    protected $providers;

    public function __construct(EntityManager $em, $container)
    {
        $this->em              =  $em;
        $this->coordinatesRepo =  $this->em->getRepository('PushGeocodeBundle:Coordinates');

        // Geocode API Providers

        $config = $container->getParameter('push_geocode');

        if (!array_key_exists('providers', $config)) {
            throw new \Exception("push_geocoder: Please configure providers", 1);
        }

        if(count($config['providers']) == 0 ) {
            throw new \Exception("push_geocoder: Please configure at least one provider", 1);
        }

        $providers = $config['providers'];

        # This feels really ugly and probably needs a different approach.

        if (array_key_exists('google', $providers)) {
            $google = $providers['google'];
            if (!array_key_exists('api_key', $google)) {
                throw new \Exception("push_geocoder: Please provide API key for Google provider", 1);
            }

            $this->providers[]   =  new GoogleProvider($google['api_key']);
        }

        if (array_key_exists('geocodeio', $providers)) {
            $geocodeio = $providers['geocodeio'];
            if (!array_key_exists('api_key', $geocodeio)) {
                throw new \Exception("push_geocoder: Please provide API key for Geocode.io provider", 1);
            }

            $this->providers[]   =  new GeocodeIOProvider($geocodeio['api_key']);
        }
    }

    /**
     * Attemps to pass a method to each provider for a given
     * geolocation service. It only iterates if data is not returned
     * from the previous provider.
     *
     * @param  method  (string)
     * @param  data    (mixed)
     * @return data    (mixed)
     */

    private function callProvider($method, $data) 
    {
        foreach ($this->providers as $provider) {
            try {
                return $provider->$method($data);
            }
            catch (NoResultsException $e) {
                $results = null;
            }
        }

        return null;
    }

    /**
     * Returns a zipcode from a physical address.
     *
     * @param  address (string)
     * @param  city    (string)
     * @param  state   (string)
     * @return zip     (string)
     */

    public function getZipFromAddress($address, $city, $state)
    {
        return $this->callProvider('getZipFromAddress', array($address, $city, $state));
    }

    /**
     * Returns a string of coordinates from a locations object.
     *
     * @param  entity       (locations object)
     * @return coordinates  (string)
     */

    public function getLocationCoords(LocationInterface $entity)
    {
        $label = $entity->getAddress() . $entity->getCity() . $entity->getState() . $entity->getZip();
        
        // Check if the coords have been cached
        $results = $this->coordinatesRepo->findOneByLabel($label);
            
        if ($results) {
            return $results->getLat() . ',' . $results->getLng();
        } 

        // If not then get the coordinates needed 

        $results = $this->callProvider('getLocationCoords', $entity);

        $coords = explode(',', $results);
        $lat = (float) $coords[0];
        $lng = (float) $coords[1];

        $coordinates = new Coordinates();
        $coordinates->setLabel($label);
        $coordinates->setLat($lat);
        $coordinates->setLng($lng);

        $this->em->persist($coordinates);
        $this->em->flush();
        
        return $results;
    }

    /**
     * Returns a string of coordinates from a given zip code.
     *
     * @param  zip          (string)
     * @return coordinates  (string)
     */

    public function getZipCoords($zip)
    {

        // Check the database to determine if the zip code being passed through
        // has it's coordinates already stored in the database.

        $zipCoords = $this->coordinatesRepo->findOneByLabel($zip);
            
        if ($zipCoords) {
            $coords = $zipCoords->getLat() . ',' . $zipCoords->getLng();
            return $coords;
        } 

        // If the database does not have the zip code, we will use the the provider
        // apis to locate it and save it to the database for future caching.

        $coords = $this->callProvider('getZipCoords', $zip);

        if ($coords) {

            $coords_array = explode(',', $coords);
         
            $coordinates = new Coordinates();
            $coordinates->setLabel($zip);
            $coordinates->setLat($coords_array[0]);
            $coordinates->setLng($coords_array[1]);

            $this->em->persist($coordinates);
            $this->em->flush();
        }
        
        return $coords;
    }


    public function coordDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {

        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
          pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }


    public function getLocationsWithinRadius($zipCoords, $locations, $radius = 15)
    {


        $zipCoords = explode(',', $zipCoords);

        // Most of this code assumes a returned value from googlemaps api. However, if malicious or jerk data 
        // is entered (such as 'asdfasdf') for the zipcode, the system craps out because it recieves an empty 
        // object from google. This checks if the data can be used and if not, exits the function which 
        // consequently returns no locations, which would be the desired result.

        if( !isset($zipCoords[1]) ) { return null; }

        // Code can be correctly parsed... proceed!

        $zipLat = (float) $zipCoords[0];
        $zipLong = (float) $zipCoords[1];

        $locList = array();

        foreach ($locations as $l) {
            $coords = $this->getLocationCoords($l);

            $coordsExpanded = explode(',', $coords);
            $lat = (float) $coordsExpanded[0];
            $lon = (float) $coordsExpanded[1];
            $dist = $this->coordDistance($zipLat, $zipLong, $lat, $lon);
            $dist = $dist * 0.000621371;
            $dist = round($dist, 1);
            if ($dist <= $radius) {
                $locList[] = array(
                    "distance" => $dist,
                    "location" => $l,
                    "coords"   => $coords,
                );
            }
        }
        $locList = $this->record_sort($locList, 'distance');
        return $locList;
    }

    public function record_sort($records, $field, $reverse=false)
    {
        $hash = array();
        foreach($records as $record)
        {
            $hash[$record[$field]] = $record;
        }
        ($reverse)? krsort($hash) : ksort($hash);
        $records = array();
        foreach($hash as $record)
        {
            $records []= $record;
        }
        return $records;
    }

}

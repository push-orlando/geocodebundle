<?php

namespace Push\GeocodeBundle\Provider;

interface ProviderInterface {

    public function getLocationCoords(LocationInterface $entity);
    public function getZipFromAddress(Array $address);
    public function getZipCoords($zip);

}
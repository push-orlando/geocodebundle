<?php

namespace Push\GeocodeBundle\Provider;

interface LocationInterface {
    public function getAddress();
    public function getCity();
    public function getState();
    public function getZip();
}
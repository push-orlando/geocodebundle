<?php

namespace Push\GeocodeBundle\Provider;

class GeocodeIOProvider implements ProviderInterface
{

    private $api_key;
    private $base_url;

    public function __construct($api_key) {
        $this->api_key = $api_key;
        $this->base_url = 'http://api.geocod.io/v1/geocode?api_key=' . $api_key . '&q=';
    }

    /**
     * Returns a string of coordinates from a locations object.
     *
     * @param  entity       (locations object)
     * @return coordinates  (string)
     */

    public function getLocationCoords(LocationInterface $entity)
    {
        $url = implode(', ', array(
            $entity->getAddress(),
            $entity->getCity(),
            $entity->getState(),
            $entity->getZip()
        ));

        $call = $this->base_url.urlencode($url);
        $results = json_decode(file_get_contents($call));

        if ($results->results) {
            $lat = $results->results[0]->location->lat;
            $lng = $results->results[0]->location->lng;
            return $lat . ',' . $lng;
        }
        
        throw new NoResultsException('No Results were returned');
    }

    /**
     * Returns a string of coordinates from a given zip code.
     *
     * @param  zip          (string)
     * @return coordinates  (string)
     */

    public function getZipCoords($zip)
    {

        $call = $this->base_url.urlencode(trim($zip));
        $results = json_decode(file_get_contents($call));

        if ($results->results) {
            $lat = $results->results[0]->location->lat;
            $lng = $results->results[0]->location->lng;
            return $lat . ',' . $lng;
        }

        throw new NoResultsException('No Results were returned');
    }

    /**
     * Returns a zipcode from a physical address.
     *
     * @param  address (array)
     * @return zip     (string)
     */

    public function getZipFromAddress(array $address) {

        $url = implode(', ', array_map('trim', $address));
        $call = $this->base_url.urlencode($url);
        $results = json_decode(file_get_contents($call));

        if ($results) {
            return $results->results[0]->address_components->zip;
        }
        
        throw new NoResultsException('No Results were returned');
    }

}
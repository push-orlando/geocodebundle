<?php

namespace Push\GeocodeBundle\Provider;

class GoogleProvider implements ProviderInterface
{

    private $api_key;
    private $base_url = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=';

    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }

    /**
     * Returns a string of coordinates from a locations object.
     *
     * @param  entity       (locations object)
     * @return coordinates  (string)
     */

    public function getLocationCoords(LocationInterface $entity)
    {
        $url = implode(', ', array(
            $entity->getAddress(),
            $entity->getCity(),
            $entity->getState(),
            $entity->getZip()
        ));

        $call = $this->base_url.urlencode($url);
        $results = json_decode(file_get_contents($call));

        if ($results->results) {
            $lat = $results->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $lng = $results->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
            return $lat . ',' . $lng;
        }

        throw new NoResultsException('No Results were returned');
    }

    /**
     * Returns a string of coordinates from a given zip code.
     *
     * @param  zip          (string)
     * @return coordinates  (string)
     */

    public function getZipCoords($zip)
    {

        $call = $this->base_url.urlencode(trim($zip));
        $results = json_decode(file_get_contents($call));

        if( $results->results ) {
            $lat = $results->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $long = $results->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
            return $lat . ',' . $long;
        }

        throw new NoResultsException('No Results were returned');
    }

    /**
     * Returns a zipcode from a physical address.
     *
     * @param  address (array)
     * @return zip     (string)
     */

    public function getZipFromAddress(array $address)
    {

        $url = implode(', ', array_map('trim', $address));
        $call = $this->base_url.urlencode($url);
        $results = json_decode(file_get_contents($call));

        if ($results->results) {
            $results = $results->results[0]->address_components;
            return $results[ count($results) - 1]->long_name;
        }
        
        throw new NoResultsException('No Results were returned');
    }

}